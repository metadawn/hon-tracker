package org.hontracker;

import java.util.Collections;
import java.util.Comparator;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Vector;
import java.util.concurrent.LinkedBlockingQueue;

import org.hontracker.Global.State;
import org.hontracker.Player.GameMode;
import org.hontracker.activities.IHonTrackerUI;
import org.hontracker.activities.Main;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.lorecraft.phparser.SerializedPhpParser;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.AsyncTask;
import android.util.Log;
import android.view.ViewGroup;

public class PlayerManager extends SQLiteOpenHelper {
    private static PlayerManager instance = null;

    // Database data
    private static final String DatabaseName = Global.TAG + ".db";
    private static final int DatabaseVersion = 7;
    private SQLiteDatabase db;
    private LinkedBlockingQueue<PlayerData> queuedUpdates = new LinkedBlockingQueue<PlayerData>();
    private Thread retrievePlayers = null;
    private boolean dbLoadInBackground = true;

    private Hashtable<String, PlayerData> playersByName = new Hashtable<String, PlayerData>();
    private PlayerData starredPlayer = null;
    private boolean backgroundFetchInProgress = false;

    public enum SortType {
        Name, Rating, KDR, TSR
    }

    public class PlayerData {
        public Player player = null;
        public PlayerView playerView = null;
        public boolean onlyUnpopulated = false;
        public SortType sortType = SortType.Name;
    }

    public static PlayerManager getInstance(Context context) {
        if (instance == null) {
            instance = new PlayerManager(context);
        }
        return instance;
    }

    public void releaseInstance() {
        retrievePlayers.interrupt();
        instance = null;
    }

    private PlayerManager(Context context) {
        super(context, DatabaseName, null, DatabaseVersion);
        db = getWritableDatabase();

        retrievePlayers = new Thread(new Runnable() {
            PlayerData playerData;

            @Override
            public void run() {
                retrieveGameVersion();
                while (true) {
                    try {
                        playerData = queuedUpdates.take();

                        if (backgroundOperationActive() == false) {
                            // If there was no background operation before,
                            // it means that we were waiting for players to
                            // get queued.
                            Log.v(Global.TAG,
                                    "PlayerManger.checkRemainingLoadCount.Thread.run - Retrieving player stats...");
                            backgroundFetchInProgress = true;
                            notifyBackgroundOperationStarted();
                        }
                        notifyBackgroundOperationInProgress(playerData);

                        playerData.player.retrieveData();
                    } catch (Exception e) {
                        Log.e(Global.TAG,
                                "PlayerManger.Runnable.run - " + e.toString());
                    }
                    notifyPlayerUpdateComplete(playerData);

                    if (queuedUpdates.size() == 0) {
                        Log.v(Global.TAG,
                                "PlayerManger.checkRemainingLoadCount.Thread.run - Retrieve complete.");
                        backgroundFetchInProgress = false;
                        notifyBackgroundOperationComplete();
                    }
                }
            }
        });
        loadPlayers();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE PlayerIndex (Name TEXT PRIMARY KEY)");
        // The Players table is a cache of values
        Player.dbOnCreate(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion == 1) {
            db.execSQL("DROP TABLE PlayerIndex");
            db.execSQL("CREATE TABLE PlayerIndex (Name TEXT PRIMARY KEY)");

            // Populate Index from Players table
            Cursor cursor = db.query("Players", new String[] { "Name" }, null,
                    null, null, null, "Name asc");
            if (cursor.moveToFirst()) {
                do {
                    String name = cursor.getString(0);
                    db.execSQL("INSERT INTO PlayerIndex values ('" + name
                            + "')");
                } while (cursor.moveToNext());
            }
            cursor.close();
        }
        Player.dbOnUpdate(db, oldVersion, newVersion);
    }

    private class PlayerLoadDataTask extends
            AsyncTask<LinkedList<PlayerData>, Void, Void> {
        @Override
        protected Void doInBackground(LinkedList<PlayerData>... params) {
            final LinkedList<PlayerData> playersToLoad = params[0];

            notifyBackgroundOperationStarted();
            loadPlayerData(playersToLoad);
            notifyBackgroundOperationComplete();
            return null;
        }
    }

    @SuppressWarnings("unchecked")
    private void loadPlayers() {
        Vector<String> players = new Vector<String>();
        Cursor cursor = db.query("PlayerIndex", null, null, null, null, null,
                "Name asc");
        if (cursor.moveToFirst()) {
            do {
                players.add(cursor.getString(0));
            } while (cursor.moveToNext());
        }
        cursor.close();

        Iterator<String> iterator = players.iterator();
        LinkedList<PlayerData> playersToLoad = new LinkedList<PlayerData>();
        if (iterator.hasNext()) {
            while (iterator.hasNext()) {
                String name = iterator.next();
                PlayerData playerData = new PlayerData();
                playerData.player = new Player(name, db);
                playersByName.put(name, playerData);

                playersToLoad.add(playerData);
            }

            if (dbLoadInBackground == true) {
                new PlayerLoadDataTask().execute(playersToLoad);
            } else {
                loadPlayerData(playersToLoad);
            }
        } else {
            // Start player processing thread as database load is complete.
            retrievePlayers.start();
        }
    }

    private void loadPlayerData(final LinkedList<PlayerData> playersToLoad) {
        // Load data for all players first.
        Iterator<PlayerData> iterator = playersToLoad.iterator();
        while (iterator.hasNext()) {
            final PlayerData playerData = iterator.next();
            playerData.player.loadData();
            if (playerData.player.getStarred() == true) {
                starredPlayer = playerData;
            }
        }

        Log.d(Global.TAG, "PlayerManager.loadPlayerData - Load completed.");

        LinkedList<PlayerData> queuedPlayers = new LinkedList<PlayerData>();
        // Mark each player as being queued.
        while (queuedUpdates.peek() != null) {
            try {
                PlayerData playerData = queuedUpdates.take();
                // Only queue players that need to be updated.
                if (playerData.onlyUnpopulated == false
                        || playerData.onlyUnpopulated == true
                        && playerData.player.getPopulated() == false) {
                    queuedPlayers.add(playerData);
                    playerData.player.setStateQueued();
                }
            } catch (InterruptedException e) {
                Log.e(Global.TAG,
                        "PlayerManager.loadPlayerData - " + e.toString());
            }
        }
        // Add players to the queue.
        while (queuedPlayers.isEmpty() == false) {
            queuedUpdates.add(queuedPlayers.remove());
        }
        // Start player processing thread as database load is complete.
        retrievePlayers.start();
    }
    
    @SuppressWarnings("unchecked")
    private void retrieveGameVersion() {
        LinkedHashMap<Object, Object> result = null;
        int retryCount = Global.RETRY_COUNT;
        while (retryCount > 0) {
            try {
                final String url = "http://" + Player.getServer()
                        + "/patcher/patcher.php";
                Document document = Jsoup
                        .connect(url)
                        .data("version", "0.0.0.0", "os", "mac",
                                "arch", "universal")
                        .userAgent(Player.getUserAgent()).post();
                retryCount = 0; // Fetch successful

                String data = document.body().text();

                SerializedPhpParser serializedPhpParser = new SerializedPhpParser(
                        data);
                result = (LinkedHashMap<Object, Object>) serializedPhpParser
                        .parse();
                String retrievedGameVersion = (String) result.get("version");
                if(retrievedGameVersion != null && retrievedGameVersion.length() != 0) {
                    Player.setGameVersion(retrievedGameVersion);
                    Log.i(Global.TAG,
                            "PlayerManager.retrieveGameVersion - Successfully retrieved game version \""
                                    + retrievedGameVersion + "\".");
                }
                break;
            } catch (Exception e) {
                if (--retryCount <= 0) {
                    Log.e(Global.TAG,
                            "PlayerManager.retrieveGameVersion - Failed to retrieve game version. "
                                    + e.toString());
                }
                Log.w(Global.TAG,
                        "PlayerManager.retrieveGameVersion - Retrying to retrieve game version. "
                                + e.toString());
            }
        }
    }

    public int playerCount() {
        return playersByName.size();
    }

    public boolean playerKnown(String name) {
        boolean rc = false;
        if (playersByName.get(name) != null) {
            rc = true;
        }
        return rc;
    }

    public boolean addPlayer(String name) {
        boolean rc = false;
        if (playerKnown(name) == false) {
            PlayerData playerData = new PlayerData();
            playerData.player = new Player(name, db);
            // Add to Index
            db.execSQL("INSERT INTO PlayerIndex values ('" + name + "')");
            // Add to Cache
            playerData.player.dbAdd();

            playersByName.put(name, playerData);

            updatePlayer(name);

            rc = true;
        }
        return rc;
    }

    public boolean removePlayer(String name) {
        boolean rc = false;
        PlayerData playerData = playersByName.get(name);
        if (playerData != null) {
            // Remove from starred player
            if (starredPlayer != null
                    && starredPlayer.player.getName() == playerData.player
                            .getName()) {
                starredPlayer = null;
            }
            // Remove from update queue
            queuedUpdates.remove(playerData);
            // Remove from Index
            playersByName.remove(name);
            db.execSQL("DELETE FROM PlayerIndex where Name = '"
                    + name.toString() + "'");
            // Remove from Cache
            playerData.player.dbRemove();
            // Remove view
            if (playerData.playerView != null) {
                playerData.playerView.removeFromParent();
            }
            rc = true;
        }
        return rc;
    }

    public void populateAllPlayerViews(Context context,
            ViewGroup listItemsView, ViewGroup starredPlayerView,
            SortType sortType, boolean isAscending, boolean groupByClanTag,
            GameMode gameMode, boolean orientationHorizontal) {
        Vector<PlayerData> sortedPlayers = getSortedPlayers(sortType,
                isAscending, groupByClanTag, gameMode, true);
        Iterator<PlayerData> i = sortedPlayers.iterator();
        while (i.hasNext()) {
            PlayerData playerData = i.next();
            playerData.sortType = sortType;
            // Try to reuse view
            if (playerData.playerView == null
                    || playerData.playerView.isOrientationHorizontal() != orientationHorizontal) {
                playerData.playerView = new PlayerView(this, playerData.player,
                        context, gameMode, orientationHorizontal);
            } else {
                playerData.playerView.populateStats(gameMode);
            }

            if (playerData.player.getStarred() == true) {
                playerData.playerView.addToView(starredPlayerView);
            } else {
                playerData.playerView.addToView(listItemsView);
            }
        }
    }

    public void updatePlayers(boolean onlyUnpopulated, SortType sortType,
            boolean isAscending, boolean groupByClanTag, GameMode gameMode) {
        Log.d(Global.TAG, "PlayerManager.updatePlayers - onlyUnpopulated = "
                + new Boolean(onlyUnpopulated).toString() + ".");
        if (starredPlayer != null) {
            starredPlayer.onlyUnpopulated = onlyUnpopulated;
            addPlayerToQueue(starredPlayer);
        }
        Iterator<PlayerData> iterator = getSortedPlayers(sortType, isAscending,
                groupByClanTag, gameMode, false).iterator();
        while (iterator.hasNext()) {
            PlayerData playerData = iterator.next();
            playerData.onlyUnpopulated = onlyUnpopulated;
            addPlayerToQueue(playerData);
        }
    }

    public void updatePlayer(String name) {
        Log.d(Global.TAG, "PlayerManager.updatePlayer - '" + name + "'.");
        PlayerData playerData = playersByName.get(name);
        playerData.onlyUnpopulated = false;
        addPlayerToQueue(playerData);
    }

    private synchronized void addPlayerToQueue(PlayerData playerData) {
        if (playerData != null
                && playerData.player.getState() != State.InProgress
                && queuedUpdates.contains(playerData) == false) {
            if (playerData.onlyUnpopulated == false
                    || playerData.onlyUnpopulated == true
                    && playerData.player.getPopulated() == false) {
                if (queuedUpdates.add(playerData) == false) {
                    Log.e(Global.TAG,
                            "PlayerManager.addPlayerToQueue - Failed to add '"
                                    + playerData.player.getName()
                                    + "' to queue.");
                }
                playerData.player.setStateQueued();
                // View might not have been created
                if (playerData.playerView != null) {
                    playerData.playerView.reflectState(playerData.player
                            .getState());
                }
            }
        }
    }

    private Vector<PlayerData> getSortedPlayers(final SortType sortType,
            final boolean ascending, final boolean groupByClanTag,
            final GameMode gameMode, boolean includeStarred) {

        class PlayerComparer implements Comparator<PlayerData> {
            @Override
            public int compare(PlayerData player1, PlayerData player2) {
                int rc = player1.player.getClanTag().compareToIgnoreCase(
                        player2.player.getClanTag());
                if (!groupByClanTag || rc == 0) {
                    boolean fallbackToName = false;
                    switch (sortType) {
                    case Name:
                        rc = player1.player.getName().compareToIgnoreCase(
                                player2.player.getName());
                        break;
                    case Rating:
                        rc = player1.player.getRating(gameMode).compareTo(
                                player2.player.getRating(gameMode));
                        if (rc == 0) {
                            fallbackToName = true;
                        }
                        break;
                    case KDR:
                        Float kdr1;
                        Float kdr2;

                        kdr1 = player1.player.getDeaths(gameMode) != 0 ? player1.player
                                .getKills(gameMode).floatValue()
                                / player1.player.getDeaths(gameMode)
                                        .floatValue() : 0;
                        kdr2 = player2.player.getDeaths(gameMode) != 0 ? player2.player
                                .getKills(gameMode).floatValue()
                                / player2.player.getDeaths(gameMode)
                                        .floatValue() : 0;

                        rc = kdr1.compareTo(kdr2);
                        if (rc == 0) {
                            fallbackToName = true;
                        }
                        break;
                    case TSR:
                        rc = player1.player.getTSR(gameMode).compareTo(
                                player2.player.getTSR(gameMode));
                        if (rc == 0) {
                            fallbackToName = true;
                        }
                        break;
                    default:
                        Log.e(Global.TAG,
                                "PlayerManager.getSortedPlayers.PlayerComparer.compare - unknown sort type '"
                                        + sortType.toString() + "'.");
                        rc = player1.player.getName().compareToIgnoreCase(
                                player2.player.getName());
                        break;
                    }
                    rc = ascending ? rc : rc * -1;
                    if (fallbackToName) {
                        rc = player1.player.getName().compareToIgnoreCase(
                                player2.player.getName());
                    }
                }
                return rc;
            }
        }

        Vector<PlayerData> sortedPlayers = new Vector<PlayerData>();
        Iterator<PlayerData> iterator = playersByName.values().iterator();
        while (iterator.hasNext()) {
            PlayerData playerData = iterator.next();
            if (includeStarred == true
                    || playerData.player.getStarred() == false) {
                sortedPlayers.add(playerData);
            }
        }
        Collections.sort(sortedPlayers, new PlayerComparer());
        return sortedPlayers;
    }

    public void togglePlayerStar(String name) {
        final PlayerData previousStarredPlayer = starredPlayer;
        if (starredPlayer != null) {
            // Unstar current player
            starredPlayer.player.setStarred(false);
            starredPlayer = null;
        }
        if (previousStarredPlayer == null
                || previousStarredPlayer.player.getName() != name) {
            starredPlayer = playersByName.get(name);
            if (starredPlayer != null) {
                // Star new player
                starredPlayer.player.setStarred(true);
            } else {
                Log.e(Global.TAG,
                        "PlayerManager.togglePlayerStar - Unknown player name '"
                                + name + "'.");
            }
        }
        // Notify GUI that background operation started
        final IHonTrackerUI iHonTrackerUI = Main.getUI();
        if (iHonTrackerUI != null) {
            iHonTrackerUI.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (previousStarredPlayer != null) {
                        previousStarredPlayer.playerView.reflectStar();
                    }
                    if (starredPlayer != null) {
                        starredPlayer.playerView.reflectStar();
                        // Global.animateGrow(starredPlayer.playerView,
                        // starredPlayer.playerView.getContext());
                    }
                    iHonTrackerUI.populatePlayerStats();
                }
            });
        }
    }

    public boolean backgroundOperationActive() {
        return backgroundFetchInProgress;
    }

    private void notifyBackgroundOperationStarted() {
        // Notify GUI that background operation started
        final IHonTrackerUI iHonTrackerUI = Main.getUI();
        if (iHonTrackerUI != null) {
            iHonTrackerUI.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    iHonTrackerUI.backgroundOperationStarted();
                }
            });
        }
    }

    private void notifyBackgroundOperationInProgress(final PlayerData playerData) {
        // Notify Player View that it is in progress.
        final IHonTrackerUI iHonTrackerUI = Main.getUI();
        if (iHonTrackerUI != null) {
            iHonTrackerUI.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (playerData.playerView != null) {
                        playerData.playerView.reflectState(State.InProgress);
                    }
                }
            });
        }
    }

    private void notifyPlayerUpdateComplete(final PlayerData playerData) {
        final IHonTrackerUI iHonTrackerUI = Main.getUI();
        if (iHonTrackerUI != null && playerData != null) {
            iHonTrackerUI.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (playerData.playerView != null) {
                        playerData.playerView.populateStats(iHonTrackerUI
                                .getGameMode());
                    }
                }
            });
        }
    }

    private void notifyBackgroundOperationComplete() {
        // Notify the GUI that all updates have finished
        final IHonTrackerUI iHonTrackerUI = Main.getUI();
        if (iHonTrackerUI != null) {
            iHonTrackerUI.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    iHonTrackerUI.backgroundOperationCompleted();
                }
            });
        }
    }

}