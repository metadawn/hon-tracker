package org.hontracker;

import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;

public class HonTrackerAnimationListener implements AnimationListener {
    private View view;
    private boolean removeView;

    public HonTrackerAnimationListener(View view, boolean removeView) {
        this.view = view;
        this.removeView = removeView;
    }

    @Override
    public void onAnimationStart(Animation animation) {
    }

    @Override
    public void onAnimationRepeat(Animation animation) {
    }

    @Override
    public void onAnimationEnd(Animation animation) {
        Global.hideView(view, removeView);
    }
}
