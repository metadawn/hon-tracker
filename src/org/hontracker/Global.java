package org.hontracker;

import java.io.File;
import java.io.FileOutputStream;

import android.content.Context;
import android.graphics.Rect;
import android.os.Environment;
import android.os.Vibrator;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

public class Global {

    static final public String TAG = "HonTracker";
    static final public int RETRY_COUNT = 3;
    static final public int VIBRATE_LENGTH = 25;
    static final public int BACKGROUND_COLOR = 0x7f131622;
    static final public int BACKGROUND_NULL = 0;

    public enum State {
        Idle, Queued, InProgress, Incomplete
    }

    static public void hapticFeedback(final Context context) {
        ((Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE))
                .vibrate(Global.VIBRATE_LENGTH);
    }

    static public void animateFadeIn(final View view, Context context) {
        if (view == null) {
            return;
        }
        Animation shrink = AnimationUtils
                .loadAnimation(context, R.anim.fade_in);
        view.startAnimation(shrink);
        view.setVisibility(View.VISIBLE);
    }

    static public void animateFadeOut(final View view, Context context,
            final boolean removeView) {
        // Only animate if the view is visible.
        if (validateView(view) == true) {
            Animation fadeOut = AnimationUtils.loadAnimation(context,
                    R.anim.fade_out);
            fadeOut.setAnimationListener(new HonTrackerAnimationListener(view,
                    removeView));
            view.startAnimation(fadeOut);
        } else {
            hideView(view, removeView);
        }
    }

    static public void animateShrink(final View view, Context context,
            final boolean removeView) {
        // Only animate if the view is visible.
        if (validateView(view) == true) {
            Animation shrink = AnimationUtils.loadAnimation(context,
                    R.anim.shrink);
            shrink.setAnimationListener(new HonTrackerAnimationListener(view,
                    removeView));
            view.startAnimation(shrink);
        } else {
            hideView(view, removeView);
        }
    }

    static public void animateGrow(final View view, Context context) {
        if (view == null) {
            return;
        }
        view.startAnimation(AnimationUtils.loadAnimation(context, R.anim.grow));
    }

    static public void hideView(final View view, boolean removeView) {
        if (removeView == true) {
            view.setVisibility(View.GONE);
        } else {
            view.setVisibility(View.INVISIBLE);
        }
    }

    static private boolean validateView(final View view) {
        boolean result = false;
        if (view != null && view.getVisibility() == View.VISIBLE) {
            Rect visibleRect = new Rect();
            view.getGlobalVisibleRect(visibleRect);
            if (visibleRect.height() != 0) {
                result = true;
            }
        }
        return result;
    }

    static public void storeHtml(String html) throws Exception {
        File path = new File(Environment.getExternalStorageDirectory(),
                Global.TAG);
        File file = new File(path, "stats.html");

        try {
            // Create directory structure
            if (!file.exists() && !file.mkdirs()) {
                throw new Exception("Directory not created");
            }

            // Create the file, delete any existing one
            if (file.exists()) {
                file.delete();
            }
            file.createNewFile();

            // Write out the html to disk
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            fileOutputStream.write(html.getBytes(), 0, html.getBytes().length);
            fileOutputStream.flush();
            fileOutputStream.close();
        } catch (Exception e) {
            Log.e(Global.TAG, "Global.storeHtml - " + e.toString());
            throw e;
        }
    }
}
