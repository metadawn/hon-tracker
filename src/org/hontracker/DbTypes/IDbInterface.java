package org.hontracker.DbTypes;

import java.util.Iterator;
import java.util.LinkedHashMap;

import org.hontracker.Utils;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public abstract class IDbInterface {
    protected abstract String getTableName();

    protected abstract String getPrimaryKeyValue();

    protected LinkedHashMap<String, DbType<?>> properties = getProperties(getPrimaryKeyValue());

    public void dbOnCreate(SQLiteDatabase db) {
        Log.d(Utils.TAG + " IDbInterface.dbOnCreate", "Creating database.");
        StringBuilder query = new StringBuilder("CREATE TABLE "
                + getTableName() + " (");
        Iterator<DbType<?>> propertiesIterator = getProperties("").values()
                .iterator();
        while (propertiesIterator.hasNext()) {
            DbType<?> property = propertiesIterator.next();
            query.append(property.getColumnDefinition());
            if (propertiesIterator.hasNext()) {
                query.append(", ");
            }
        }
        query.append(")");
        db.execSQL(query.toString());
    }

    public void dbOnUpdate(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.d(Utils.TAG + " IDbInterface.dbOnUpdate",
                "Updating database. oldVersion = '"
                        + String.valueOf(oldVersion) + "', newVersion = '"
                        + String.valueOf(newVersion) + "'.");
        db.execSQL("DROP TABLE " + getTableName());
        dbOnCreate(db);
    }

    public long dbAdd(SQLiteDatabase db) {
        long result = db.insert(getTableName(), null, generateValues());
        Log.d(Utils.TAG + " IDbInterface.dbAdd", "Adding '"
                + getPrimaryKeyValue() + "' to table '" + getTableName()
                + "', result = '" + String.valueOf(result) + "'.");
        return result;
    }

    public abstract long dbRemove(SQLiteDatabase db);

    public abstract void dbUpdate(SQLiteDatabase db);

    public abstract void dbLoad();

    public abstract LinkedHashMap<String, DbType<?>> getProperties(
            String primaryKeyValue);

    public ContentValues generateValues() {
        ContentValues values = new ContentValues();
        Iterator<DbType<?>> propertiesIterator = properties.values().iterator();
        while (propertiesIterator.hasNext()) {
            propertiesIterator.next().fillContentValue(values);
        }

        return values;
    }
}
