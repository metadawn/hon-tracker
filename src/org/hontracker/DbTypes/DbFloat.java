package org.hontracker.DbTypes;

import android.content.ContentValues;
import android.database.Cursor;

public class DbFloat extends DbType<Float> {
    private static final Type type = Type.REAL;
    private Float value = new Float(0);

    public DbFloat(String dbName, Float value, String options) {
        super(dbName, type, options);
        this.setValue(value);
    }

    public DbFloat(String dbName, Float value) {
        super(dbName, type, "");
        this.setValue(value);
    }

    public DbFloat(String dbName) {
        super(dbName, type, "");
        this.setValue(0f);
    }

    @Override
    public void fillContentValue(ContentValues values) {
        values.put(getDbName(), getValue());
    }

    @Override
    public void retrieveValue(final Cursor cursor) {
        setValue(cursor.getFloat(getColumnIndex(cursor)));
    }

    @Override
    public int setValue(Float newValue) {
        int comparison = 0;
        if (newValue == null) {
            newValue = new Float(0);
        }
        comparison = value.compareTo(newValue);
        if (comparison != 0) {
            value = newValue;
        }
        return comparison;
    }

    @Override
    public Float getValue() {
        return value;
    }

    @Override
    public String toString() {
        return value.toString();
    }
}
