package org.hontracker.DbTypes;

import android.content.ContentValues;
import android.database.Cursor;

public class DbBoolean extends DbType<Boolean> {
    private static final Type type = Type.INTEGER;
    private Boolean value = new Boolean(false);

    public DbBoolean(String dbName, Boolean value, String options) {
        super(dbName, type, options);
        this.setValue(value);
    }

    public DbBoolean(String dbName, Boolean value) {
        super(dbName, type, "");
        this.setValue(value);
    }

    public DbBoolean(String dbName) {
        super(dbName, type, "");
        this.setValue(false);
    }

    @Override
    public void fillContentValue(ContentValues values) {
        // Stored in the database as an Integer.
        Integer contentValue = 0;
        if (getValue().booleanValue() != false) {
            contentValue = 1;
        }
        values.put(getDbName(), contentValue);
    }

    @Override
    public void retrieveValue(final Cursor cursor) {
        setValue(cursor.getInt(getColumnIndex(cursor)));
    }

    @Override
    public int setValue(Boolean newValue) {
        int comparison = 0;
        if (newValue == null) {
            newValue = new Boolean(false);
        }
        comparison = value.compareTo(newValue);
        if (comparison != 0) {
            value = newValue;
        }
        return comparison;
    }

    public int setValue(Integer newValue) {
        int comparison = 0;
        Boolean newBoolValue;
        if (newValue == 0) {
            newBoolValue = new Boolean(false);
        } else {
            newBoolValue = new Boolean(true);
        }
        comparison = value.compareTo(newBoolValue);
        if (comparison != 0) {
            value = newBoolValue;
        }
        return comparison;
    }

    @Override
    public Boolean getValue() {
        return value;
    }

    @Override
    public String toString() {
        return value.toString();
    }
}
