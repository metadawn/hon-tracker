package org.hontracker.DbTypes;

public class DbTimestamp extends DbString {

    public DbTimestamp(String dbName) {
        super(dbName);
    }

    public DbTimestamp(String dbName, String value) {
        super(dbName, value);
    }

    public DbTimestamp(String dbName, String value, String options) {
        super(dbName, value, options);
    }
}
