package org.hontracker.DbTypes;

import android.content.ContentValues;
import android.database.Cursor;

public abstract class DbType<T> {
    protected enum Type {
        TEXT, INTEGER, REAL, BLOB
    }

    private String dbName;
    private Type dbType;
    private String options;
    private Integer columnIndex = -1;

    public DbType(String dbName, Type dbType, String options) {
        setDbName(dbName);
        setDbType(dbType);
        setOptions(options);
    }

    public abstract void fillContentValue(ContentValues values);

    public abstract void retrieveValue(final Cursor cursor);

    public abstract int setValue(T value);

    public abstract T getValue();

    @Override
    public abstract String toString();

    public final String getColumnDefinition() {
        StringBuilder sb = new StringBuilder(dbName + " " + dbType.toString());
        if (getOptions().length() > 0) {
            sb.append(" " + getOptions());
        }
        return sb.toString();
    }

    private final void setDbName(String dbName) {
        this.dbName = dbName;
    }

    public final String getDbName() {
        return dbName;
    }

    private final void setDbType(Type dbType) {
        this.dbType = dbType;
    }

    public final Type getDbType() {
        return dbType;
    }

    private final void setOptions(String options) {
        this.options = options;
    }

    public final String getOptions() {
        return options;
    }

    private final void setColumnIndex(final Cursor cursor) {
        if (columnIndex == -1) {
            columnIndex = cursor.getColumnIndexOrThrow(dbName);
        }
    }

    public final Integer getColumnIndex(final Cursor cursor) {
        setColumnIndex(cursor);
        return columnIndex;
    }
}
