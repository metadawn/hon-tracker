package org.hontracker.DbTypes;

import android.content.ContentValues;
import android.database.Cursor;

public class DbInteger extends DbType<Integer> {
    private static final Type type = Type.INTEGER;
    private Integer value = new Integer(0);

    public DbInteger(String dbName, Integer value, String options) {
        super(dbName, type, options);
        this.setValue(value);
    }

    public DbInteger(String dbName, Integer value) {
        super(dbName, type, "");
        this.setValue(value);
    }

    public DbInteger(String dbName) {
        super(dbName, type, "");
        this.setValue(0);
    }

    @Override
    public void fillContentValue(ContentValues values) {
        values.put(getDbName(), getValue());
    }

    @Override
    public void retrieveValue(final Cursor cursor) {
        setValue(cursor.getInt(getColumnIndex(cursor)));
    }

    @Override
    public int setValue(Integer newValue) {
        int comparison = 0;
        if (newValue == null) {
            newValue = new Integer(0);
        }
        comparison = value.compareTo(newValue);
        if (comparison != 0) {
            value = newValue;
        }
        return comparison;
    }

    @Override
    public Integer getValue() {
        return value;
    }

    @Override
    public String toString() {
        return value.toString();
    }
}
