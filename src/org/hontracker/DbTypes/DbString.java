package org.hontracker.DbTypes;

import android.content.ContentValues;
import android.database.Cursor;

public class DbString extends DbType<String> {
    private static final Type type = Type.TEXT;
    private String value = new String();

    public DbString(String dbName, String value, String options) {
        super(dbName, type, options);
        this.setValue(value);
    }

    public DbString(String dbName, String value) {
        super(dbName, type, "");
        this.setValue(value);
    }

    public DbString(String dbName) {
        super(dbName, type, "");
        this.setValue("");
    }

    @Override
    public void fillContentValue(ContentValues values) {
        values.put(getDbName(), getValue());
    }

    @Override
    public void retrieveValue(final Cursor cursor) {
        setValue(cursor.getString(getColumnIndex(cursor)));
    }

    @Override
    public int setValue(String newValue) {
        int comparison = 0;
        if (value == null) {
            newValue = new String();
        }
        comparison = value.compareTo(newValue);
        if (comparison != 0) {
            value = newValue;
        }
        return comparison;
    }

    @Override
    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return value.toString();
    }
}
