package org.hontracker.DbTypes;

import java.util.Arrays;

import android.content.ContentValues;
import android.database.Cursor;

public class DbByteArray extends DbType<byte[]> {
    private static final Type type = Type.BLOB;
    private byte[] value = new byte[] {};

    public DbByteArray(String dbName, byte[] value, String options) {
        super(dbName, type, options);
        this.setValue(value);
    }

    public DbByteArray(String dbName, byte[] value) {
        super(dbName, type, "");
        this.setValue(value);
    }

    public DbByteArray(String dbName) {
        super(dbName, type, "");
        this.setValue(new byte[] {});
    }

    @Override
    public void fillContentValue(ContentValues values) {
        values.put(getDbName(), getValue());
    }

    @Override
    public void retrieveValue(final Cursor cursor) {
        setValue(cursor.getBlob(getColumnIndex(cursor)));
    }

    @Override
    public int setValue(byte[] newValue) {
        int comparison = 0;
        if (newValue == null) {
            newValue = new byte[] {};
        }
        if (Arrays.equals(value, newValue)) {
            value = newValue;
            comparison = 1;
        }
        return comparison;
    }

    @Override
    public byte[] getValue() {
        return value;
    }

    @Override
    public String toString() {
        return value.toString();
    }
}
