package org.hontracker.activities;

import org.hontracker.Global;
import org.hontracker.Player.GameMode;
import org.hontracker.PlayerManager;
import org.hontracker.PlayerManager.SortType;
import org.hontracker.R;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

public class Main extends Activity implements IHonTrackerUI {
    static final private String STATE_GAME_MODE = "GameMode";
    static final private String STATE_SORT_TYPE = "SortType";
    static final private String STATE_SORT_ORDER_ASCENDING = "SortOrderAscending";
    static final private String STATE_GROUP_BY_CLAN_TAG = "GroupByClanTag";

    static private IHonTrackerUI iHonTrackerUI = null;

    static private boolean forceRetrievePlayerData = true;

    private SortType sortType = SortType.Name;
    private boolean sortOrderAscending = true;
    private boolean groupByClanTag = true;

    static private GameMode gameMode = GameMode.MM;

    private PlayerManager playerManager;
    private Animation rotate;

    private ImageButton home;
    private ImageButton refresh;
    private ImageButton search;
    private ImageButton sort;
    private ImageButton mode;
    private TextView tvMode;
    private ViewGroup listItems;
    private ViewGroup starredPlayer;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.main);

        playerManager = PlayerManager.getInstance(getApplicationContext());

        rotate = AnimationUtils.loadAnimation(this, R.anim.rotate);
        rotate.setInterpolator(new LinearInterpolator());
        rotate.setRepeatMode(Animation.RESTART);

        tvMode = (TextView) findViewById(R.id.TextViewMode);
        listItems = (ViewGroup) findViewById(R.id.ListItems);
        starredPlayer = (ViewGroup) findViewById(R.id.StarredPlayer);

        home = (ImageButton) findViewById(R.id.Home);
        home.setVisibility(View.INVISIBLE);

        refresh = (ImageButton) findViewById(R.id.Refresh);
        refresh.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Global.hapticFeedback(getApplicationContext());
                playerManager.updatePlayers(false, sortType,
                        sortOrderAscending, groupByClanTag, gameMode);
            }
        });

        search = (ImageButton) findViewById(R.id.Search);
        search.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Global.hapticFeedback(getApplicationContext());
                Intent myIntent = new Intent(Main.this, AddPlayer.class);
                startActivity(myIntent);
            }
        });

        sort = (ImageButton) findViewById(R.id.Sort);
        sort.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Global.hapticFeedback(getApplicationContext());
                final Dialog dialog = new Dialog(Main.this);

                dialog.setContentView(R.layout.sort_dialog);
                dialog.setTitle("Please choose a sort method:");

                RadioGroup rgSortType = (RadioGroup) dialog
                        .findViewById(R.id.RGSortType);
                rgSortType.clearCheck();
                switch (sortType) {
                case Name:
                    rgSortType.check(R.id.RBName);
                    break;
                case Rating:
                    rgSortType.check(R.id.RBRating);
                    break;
                case KDR:
                    rgSortType.check(R.id.RBKDR);
                    break;
                case TSR:
                    rgSortType.check(R.id.RBTSR);
                    break;
                default:
                    rgSortType.check(R.id.RBName);
                    Log.e(Global.TAG,
                            "HonTrackerMain.sortTypeOnClickListener - Unknown sort type '"
                                    + sortType.toString() + "'.");
                    break;
                }

                class SortTypeClickListener implements OnClickListener {
                    private SortType sortType;
                    private boolean isAscending;
                    private CheckBox cbGroupByClanTag;

                    public SortTypeClickListener(SortType sortType,
                            boolean isAscending, CheckBox cbGroupByClanTag) {
                        this.sortType = sortType;
                        this.isAscending = isAscending;
                        this.cbGroupByClanTag = cbGroupByClanTag;
                    }

                    @Override
                    public void onClick(View v) {
                        Global.hapticFeedback(getApplicationContext());
                        Main.this.groupByClanTag = cbGroupByClanTag.isChecked();
                        populatePlayerStats(sortType, isAscending,
                                Main.this.groupByClanTag);
                        dialog.dismiss();
                    }
                }

                CheckBox cbGroupByClanTag = (CheckBox) dialog
                        .findViewById(R.id.CBGroupClanTag);
                cbGroupByClanTag.setChecked(Main.this.groupByClanTag);

                RadioButton name = (RadioButton) dialog
                        .findViewById(R.id.RBName);
                name.setOnClickListener(new SortTypeClickListener(
                        SortType.Name, true, cbGroupByClanTag));

                RadioButton rating = (RadioButton) dialog
                        .findViewById(R.id.RBRating);
                rating.setOnClickListener(new SortTypeClickListener(
                        SortType.Rating, false, cbGroupByClanTag));
                if (gameMode == GameMode.Pub) {
                    rating.setText(R.string.player_skill_rating);
                } else {
                    rating.setText(R.string.match_making_rating);
                }

                RadioButton kdr = (RadioButton) dialog.findViewById(R.id.RBKDR);
                kdr.setOnClickListener(new SortTypeClickListener(SortType.KDR,
                        false, cbGroupByClanTag));

                RadioButton tsr = (RadioButton) dialog.findViewById(R.id.RBTSR);
                tsr.setOnClickListener(new SortTypeClickListener(SortType.TSR,
                        false, cbGroupByClanTag));

                dialog.show();
            }
        });

        mode = (ImageButton) findViewById(R.id.Mode);
        mode.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Global.hapticFeedback(getApplicationContext());
                final Dialog dialog = new Dialog(Main.this);

                dialog.setContentView(R.layout.mode_dialog);
                dialog.setTitle(R.string.please_choose_a_game_mode_);

                RadioGroup rgMode = (RadioGroup) dialog
                        .findViewById(R.id.RGMode);
                rgMode.clearCheck();
                switch (gameMode) {
                case MM:
                    rgMode.check(R.id.RBMatchMaking);
                    break;
                case CM:
                    rgMode.check(R.id.RBCasualMode);
                    break;
                case Pub:
                    rgMode.check(R.id.RBPublicGames);
                    break;
                default:
                    rgMode.check(R.id.RBMatchMaking);
                    Log.e(Global.TAG,
                            "HonTrackerMain.modeOnClickListener - Unknown game mode '"
                                    + gameMode.toString() + "'.");
                    break;
                }

                class ModeClickListener implements OnClickListener {
                    private GameMode gameMode;

                    public ModeClickListener(GameMode gameMode) {
                        this.gameMode = gameMode;
                    }

                    @Override
                    public void onClick(View v) {
                        Global.hapticFeedback(getApplicationContext());
                        applyGameMode(gameMode);
                        dialog.dismiss();
                    }
                }
                RadioButton matchMaking = (RadioButton) dialog
                        .findViewById(R.id.RBMatchMaking);
                matchMaking.setOnClickListener(new ModeClickListener(
                        GameMode.MM));

                RadioButton casualMode = (RadioButton) dialog
                        .findViewById(R.id.RBCasualMode);
                casualMode
                        .setOnClickListener(new ModeClickListener(GameMode.CM));

                RadioButton publicGames = (RadioButton) dialog
                        .findViewById(R.id.RBPublicGames);
                publicGames.setOnClickListener(new ModeClickListener(
                        GameMode.Pub));

                dialog.show();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        iHonTrackerUI = this;
        loadPreferences();
        displayGameMode();
        if (playerManager.playerCount() > 0) {
            playerManager.updatePlayers(!forceRetrievePlayerData, sortType,
                    sortOrderAscending, groupByClanTag, gameMode);
            populatePlayerStats();

            animationCheck();
        } else {
            updateStatus(getString(R.string.welcome));
        }
        forceRetrievePlayerData = false; // Only force retrieval first time
    }

    @Override
    protected void onPause() {
        super.onPause();
        savePreferences();
        iHonTrackerUI = null;
    }

    static public IHonTrackerUI getUI() {
        return iHonTrackerUI;
    }

    static public Context getContext() {
        return (Context) iHonTrackerUI;
    }

    private void loadPreferences() {
        SharedPreferences settings = getPreferences(Activity.MODE_PRIVATE);
        gameMode = GameMode.values()[settings.getInt(STATE_GAME_MODE,
                GameMode.MM.ordinal())];
        sortType = SortType.values()[settings.getInt(STATE_SORT_TYPE,
                SortType.Name.ordinal())];
        sortOrderAscending = settings.getBoolean(STATE_SORT_ORDER_ASCENDING,
                true);
        groupByClanTag = settings.getBoolean(STATE_GROUP_BY_CLAN_TAG, true);
    }

    private void savePreferences() {
        SharedPreferences.Editor settings = getPreferences(
                Activity.MODE_PRIVATE).edit();
        settings.putInt(STATE_GAME_MODE, gameMode.ordinal());
        settings.putInt(STATE_SORT_TYPE, sortType.ordinal());
        settings.putBoolean(STATE_SORT_ORDER_ASCENDING, sortOrderAscending);
        settings.putBoolean(STATE_GROUP_BY_CLAN_TAG, groupByClanTag);
        settings.commit();
    }

    private void applyGameMode(GameMode gameMode) {
        Global.hapticFeedback(getApplicationContext());
        if (Main.gameMode != gameMode) {
            Main.gameMode = gameMode;
            populatePlayerStats();
        }
        displayGameMode();
    }

    private void displayGameMode() {
        switch (gameMode) {
        case MM:
            tvMode.setText(R.string.match_making);
            break;
        case CM:
            tvMode.setText(R.string.casual_mode);
            break;
        case Pub:
            tvMode.setText(R.string.public_games);
            break;
        default:
            tvMode.setText(R.string.match_making);
            Log.e(Global.TAG,
                    "HonTrackerMain.displayGameMode - Unknown game mode '"
                            + gameMode.toString() + "'.");
            break;
        }
    }

    private void populatePlayerStats(SortType sortType, boolean isAscending,
            boolean groupByClanTag) {
        sortOrderAscending = isAscending;
        this.sortType = sortType;

        listItems.removeAllViews();

        if (isAscending == false) {
            updateStatus(getString(R.string.sorted_by_descending, sortType.toString()));
        } else {
            updateStatus(getString(R.string.sorted_by_ascending, sortType.toString()));
        }

        playerManager
                .populateAllPlayerViews(
                        getApplicationContext(),
                        listItems,
                        starredPlayer,
                        sortType,
                        isAscending,
                        groupByClanTag,
                        gameMode,
                        getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE);
    }

    private void animationCheck() {
        if (playerManager != null && playerManager.backgroundOperationActive()) {
            rotate.setRepeatCount(Animation.INFINITE);
            if (refresh != null
                    && (rotate.hasStarted() == false || rotate.hasEnded() == true)) {
                refresh.startAnimation(rotate);
            }
        } else {
            rotate.setRepeatCount(0);
        }
    }

    @Override
    public void backgroundOperationStarted() {
        animationCheck();
    }

    @Override
    public void backgroundOperationCompleted() {
        populatePlayerStats();
        animationCheck();
    }

    @Override
    public void updateStatus(final String text) {
        Log.i(Global.TAG, "HonTrackerMain.updateStatus - " + text);
    }

    @Override
    public GameMode getGameMode() {
        return gameMode;
    }

    @Override
    public void populatePlayerStats() {
        populatePlayerStats(sortType, sortOrderAscending, groupByClanTag);
    }
}
