package org.hontracker.activities;

import java.net.URLEncoder;
import java.util.Iterator;

import org.hontracker.Global;
import org.hontracker.Player;
import org.hontracker.PlayerManager;
import org.hontracker.R;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Attribute;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

public class AddPlayer extends Activity {
    static private FetchPlayersTask asyncTask = null;
    private ViewGroup foundPlayers;
    private Animation rotate;
    private ImageView refresh;
    private AddPlayer currentActivity = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.search);

        foundPlayers = (ViewGroup) findViewById(R.id.FoundPlayers);

        rotate = AnimationUtils.loadAnimation(this, R.anim.rotate);
        rotate.setInterpolator(new LinearInterpolator());
        rotate.setRepeatMode(Animation.RESTART);

        refresh = (ImageButton) findViewById(R.id.Refresh);
        refresh.setVisibility(View.INVISIBLE);

        ImageButton mode = (ImageButton) findViewById(R.id.Mode);
        mode.setVisibility(View.INVISIBLE);

        TextView textViewMode = (TextView) findViewById(R.id.TextViewMode);
        textViewMode.setVisibility(View.INVISIBLE);

        ImageButton sort = (ImageButton) findViewById(R.id.Sort);
        sort.setVisibility(View.INVISIBLE);

        ImageButton search = (ImageButton) findViewById(R.id.Search);
        search.setVisibility(View.INVISIBLE);

        ImageButton home = (ImageButton) findViewById(R.id.Home);
        home.setOnClickListener(new OnClickListener() {
            private boolean ignoreClick = false;

            @Override
            public void onClick(View v) {
                Global.hapticFeedback(getApplicationContext());
                if (ignoreClick == false) {
                    updateStatus(getString(R.string.returning_home));
                    ignoreClick = true;
                    finish();
                }
            }
        });

        final TextView playerSearch = (TextView) findViewById(R.id.PlayerSearch);
        playerSearch.setOnKeyListener(new OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_DOWN)
                        && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    performSearch(playerSearch.getText().toString());
                }
                // Returning false allows other listeners to react to the press.
                return false;
            }
        });
        playerSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                    int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                    int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                performSearch(playerSearch.getText().toString().trim());
            }
        });

        ImageButton clear = (ImageButton) findViewById(R.id.Clear);
        clear.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Global.hapticFeedback(getApplicationContext());
                playerSearch.setText("");
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        currentActivity = this;

        updateStatus(getString(R.string.please_enter_a_player_name));

        if (isRequestInProgress()) {
            asyncTask.cancel(true);
            asyncTask = null;
        }
    }

    private boolean isRequestInProgress() {
        return asyncTask != null
                && asyncTask.getStatus() == AsyncTask.Status.RUNNING;
    }

    private void performSearch(String name) {
        if (name.length() > 0) {
            updateStatus(getString(R.string.searching_for, name));
            if (isRequestInProgress()) {
                asyncTask.cancel(true);
            }
            asyncTask = new FetchPlayersTask();
            asyncTask.execute(name);
        }
    }

    class PlayerClickListener implements OnClickListener {
        private boolean ignoreClick;
        private View foundPlayerView;

        public PlayerClickListener(View foundPlayerView, boolean ignoreClick) {
            this.foundPlayerView = foundPlayerView;
            this.ignoreClick = ignoreClick;
        }

        @Override
        public void onClick(View v) {
            Global.hapticFeedback(getApplicationContext());

            if (ignoreClick == false) {
                try {
                    String name = ((TextView) foundPlayerView
                            .findViewById(R.id.Name)).getText().toString();
                    ((TextView) AddPlayer.this.findViewById(R.id.PlayerSearch))
                            .setText("");

                    PlayerManager.getInstance(getApplicationContext())
                            .addPlayer(name);

                    Global.animateShrink(
                            foundPlayerView.findViewById(R.id.Add),
                            getApplicationContext(), false);

                    updateStatus("Added '" + name + "'.");
                    ignoreClick = true;
                } catch (Exception e) {
                    Log.e(Global.TAG,
                            "AddPlayer.AddPlayerId.foundPlayerView.onClick - "
                                    + e.toString());
                }
            }
        }
    }

    private void AddPlayerId(String name) {
        View foundPlayerView = getLayoutInflater().inflate(
                R.layout.found_player, null);

        ImageButton addButton = (ImageButton) foundPlayerView
                .findViewById(R.id.Add);
        TextView nameView = (TextView) foundPlayerView.findViewById(R.id.Name);
        nameView.setText(name);

        boolean playerKnown = PlayerManager
                .getInstance(getApplicationContext()).playerKnown(name);

        PlayerClickListener playerClickListener = new PlayerClickListener(
                foundPlayerView, playerKnown);
        foundPlayerView.setOnClickListener(playerClickListener);
        addButton.setOnClickListener(playerClickListener);
        if (playerKnown == true) {
            addButton.setVisibility(View.INVISIBLE);
        }

        Global.animateFadeIn(foundPlayerView, getApplicationContext());
        foundPlayers.addView(foundPlayerView);
    }

    private class FetchPlayersTask extends AsyncTask<String, String, Long> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (currentActivity != null) {
                currentActivity.foundPlayers.removeAllViews();
                currentActivity.backgroundOperationStarted();
            }
        }

        @Override
        protected Long doInBackground(String... playerNames) {
            Long playersFound = new Long(0);

            String playerName = playerNames[0];
            if (Player.isRandomPlayer(playerName)) {
                publishProgress(playerName);
                ++playersFound;
            } else {
                try {
                    playerName = URLEncoder.encode(playerName, "UTF-8");
                } catch (Exception e) {
                    Log.e(Global.TAG,
                            "AddPlayer.FetchPlayersTask.doInBackground - "
                                    + e.toString());
                }
                StringBuilder url = new StringBuilder(
                        "http://www.heroesofnewerth.com/accounts_stats.php?search=");
                url.append(playerName);
                url.append("&id=autocomplete_playername");

                int retryCount = Global.RETRY_COUNT;
                while (retryCount > 0) {
                    try {
                        Document basics = Jsoup.connect(url.toString()).get();
                        Elements aElements = basics.getElementsByTag("a");
                        Iterator<Element> elementsItr = aElements.iterator();
                        while (elementsItr.hasNext()) {
                            if (isCancelled()) {
                                return playersFound;
                            }
                            Attribute attribute = elementsItr.next()
                                    .attributes().iterator().next();
                            String findIn = attribute.getValue();
                            final String searchString = "value='";
                            int nameStartIndex = findIn.indexOf(searchString)
                                    + searchString.length();
                            int nameEndIndex = findIn.indexOf("'",
                                    nameStartIndex);
                            String name = findIn.substring(nameStartIndex,
                                    nameEndIndex);

                            publishProgress(name);
                            ++playersFound;
                        }
                        break;
                    } catch (Exception e) {
                        Log.e(Global.TAG,
                                "AddPlayer.FetchPlayersTask.doInBackground - "
                                        + e.toString());
                        // Retry to fetch data on failure.
                        --retryCount;
                        if (retryCount <= 0) {
                            publishProgress((String) null);
                        }
                    }
                }
            }
            return playersFound;
        }

        @Override
        protected void onProgressUpdate(String... playerNames) {
            if (playerNames[0] == null) {
                if (currentActivity != null) {
                    currentActivity.updateStatus("Search failed.");
                }
            } else {
                Log.d(Global.TAG,
                        "AddPlayer.FetchPlayersTask.onProgressUpdate - Found '"
                                + playerNames[0] + "'.");
                if (currentActivity != null) {
                    currentActivity.AddPlayerId(playerNames[0]);
                }
            }
        }

        @Override
        protected void onPostExecute(Long result) {
            backgroundOperationCompleted();
            if (currentActivity != null) {
                currentActivity.updateStatus(getString(R.string.search_complete, result.toString()));
            }
        }
    }

    private void backgroundOperationCompleted() {
        rotate.setRepeatCount(0);
        refresh.setVisibility(View.INVISIBLE);
    }

    private void backgroundOperationStarted() {
        rotate.setRepeatCount(Animation.INFINITE);

        refresh.setVisibility(View.VISIBLE);
        refresh.startAnimation(rotate);
    }

    private void updateStatus(final String text) {
        Log.i(Global.TAG, "AddPlayer.updateStatus - " + text);
    }
}
