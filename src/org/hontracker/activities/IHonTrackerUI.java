package org.hontracker.activities;

import org.hontracker.Player.GameMode;

public interface IHonTrackerUI {

    public void backgroundOperationStarted();

    public void backgroundOperationCompleted();

    public void updateStatus(final String text);

    public GameMode getGameMode();

    public void runOnUiThread(Runnable action);

    public void populatePlayerStats();
}
