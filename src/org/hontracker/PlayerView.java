package org.hontracker;

import java.text.DecimalFormat;

import org.hontracker.Global.State;
import org.hontracker.Player.GameMode;
import org.hontracker.activities.Main;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Rect;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

public class PlayerView extends RelativeLayout {
    private Context context;
    private boolean orientationHorizontal;
    private boolean ignoreRemoveClick = false;
    private String playerUpdatedTimestamp = "Unknown";
    private GameMode previousGameMode = null;
    private Player player;

    private View playerView;
    private View vBorderWLD;
    private View vBorderRating;
    private View vBorderTSR;
    private View vBorderKDR;
    private ViewGroup vgDetails;
    private TextView tvName;
    private TextView tvPlayerStatus;
    private TextView tvRatingText;
    private TextView tvRating;
    private TextView tvKDRText;
    private TextView tvKDR;
    private TextView tvTSRText;
    private TextView tvTSR;
    private TextView tvWins;
    private TextView tvLosses;
    private TextView tvDisconnects;
    private TextView tvKills;
    private TextView tvDeaths;
    private TextView tvAssists;
    private TextView tvKADR;
    private TextView tvAPM;
    private TextView tvXPPM;
    private TextView tvGPM;
    private TextView tvWPM;
    private ImageButton ibToggle;
    private ImageButton ibRemove;
    private ImageButton ibStar;
    private ImageButton ibMatches;
    private ImageButton ibClan;

    private class ToggleDetailsClickListener implements OnClickListener {
        @Override
        public void onClick(View v) {
            if (orientationHorizontal == true) {
                return;
            }
            Global.hapticFeedback(context);
            if (player.getPopulated() == false) {
                return;
            }
            player.setExpandedView(!player.getExpandedView());
            showDetails();
            if (player.getState() != State.InProgress) {
                player.dbUpdate();
            }
        }
    }

    public PlayerView(final PlayerManager playerManager, Player player,
            final Context context, GameMode gameMode,
            boolean orientationHorizontal) {
        super(context);
        this.player = player;
        this.context = context;
        this.orientationHorizontal = orientationHorizontal;
        this.previousGameMode = gameMode;

        LayoutParams params = new LayoutParams(LayoutParams.FILL_PARENT,
                LayoutParams.WRAP_CONTENT);
        setLayoutParams(params);

        LayoutInflater.from(context).inflate(R.layout.stats_display, this);
        playerView = findViewById(R.id.PlayerView);

        vBorderWLD = findViewById(R.id.BorderWLD);
        vBorderRating = findViewById(R.id.BorderRating);
        vBorderTSR = findViewById(R.id.BorderTSR);
        vBorderKDR = findViewById(R.id.BorderKDR);

        ToggleDetailsClickListener toggleDetailsClickListener = new ToggleDetailsClickListener();
        this.setOnClickListener(toggleDetailsClickListener);

        vgDetails = (ViewGroup) findViewById(R.id.Details);
        if (player.getExpandedView() == false && orientationHorizontal != true) {
            vgDetails.setVisibility(View.GONE);
        } else {
            vgDetails.setVisibility(View.VISIBLE);
        }

        tvName = (TextView) findViewById(R.id.Player);

        tvPlayerStatus = (TextView) findViewById(R.id.PlayerStatus);

        tvRatingText = (TextView) findViewById(R.id.TextViewRating);
        tvRating = (TextView) findViewById(R.id.Rating);
        tvKDRText = (TextView) findViewById(R.id.TextViewKDR);
        tvKDR = (TextView) findViewById(R.id.KDR);
        tvTSRText = (TextView) findViewById(R.id.TextViewTSR);
        tvTSR = (TextView) findViewById(R.id.TSR);
        tvWins = (TextView) findViewById(R.id.Wins);
        tvLosses = (TextView) findViewById(R.id.Losses);
        tvDisconnects = (TextView) findViewById(R.id.Disconnects);
        tvKills = (TextView) findViewById(R.id.Kills);
        tvDeaths = (TextView) findViewById(R.id.Deaths);
        tvAssists = (TextView) findViewById(R.id.Assists);
        tvKADR = ((TextView) findViewById(R.id.KADR));
        tvAPM = (TextView) findViewById(R.id.APM);
        tvXPPM = (TextView) findViewById(R.id.XPPM);
        tvGPM = (TextView) findViewById(R.id.GPM);
        tvWPM = (TextView) findViewById(R.id.WPM);

        ibToggle = (ImageButton) findViewById(R.id.Toggle);
        ibToggle.setOnClickListener(toggleDetailsClickListener);

        ibRemove = (ImageButton) findViewById(R.id.Remove);
        ibRemove.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Global.hapticFeedback(context);
                if (ignoreRemoveClick == false) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(Main
                            .getContext());
                    builder.setMessage(
                            getContext().getString(R.string.remove_player,
                                    tvName.getText().toString()))
                            .setPositiveButton(R.string.yes,
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(
                                                DialogInterface dialog, int id) {
                                            playerManager
                                                    .removePlayer(PlayerView.this.player
                                                            .getName());
                                            Main.getUI()
                                                    .updateStatus(
                                                            getContext()
                                                                    .getString(
                                                                            R.string.removed_player,
                                                                            tvName.getText()));
                                            ignoreRemoveClick = true;
                                        }
                                    })
                            .setNegativeButton(R.string.no,
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(
                                                DialogInterface dialog, int id) {
                                            dialog.cancel();
                                        }
                                    });
                    AlertDialog alert = builder.create();
                    alert.show();
                }
            }
        });
        ibRemove.setOnLongClickListener(new OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                Global.hapticFeedback(context);
                if (ignoreRemoveClick == false) {
                    playerManager.removePlayer(PlayerView.this.player.getName());
                    Main.getUI().updateStatus(
                            getContext().getString(R.string.removed_player,
                                    tvName.getText().toString()));
                    ignoreRemoveClick = true;
                }
                return true;
            }
        });

        ibStar = (ImageButton) findViewById(R.id.Star);
        ibStar.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Global.hapticFeedback(context);
                playerManager.togglePlayerStar(PlayerView.this.player.getName());
            }
        });

        ibMatches = (ImageButton) findViewById(R.id.Matches);
        ibMatches.setOnClickListener(toggleDetailsClickListener);

        ibClan = (ImageButton) findViewById(R.id.Clan);
        ibClan.setOnClickListener(toggleDetailsClickListener);

        populateStats(gameMode);
        setVisibility(View.VISIBLE);
    }

    private void showDetails() {
        if (player.getPopulated() == false) {
            // We don't have the numbers for this player.
            if (orientationHorizontal == true) {
                vgDetails.setVisibility(View.INVISIBLE);
            } else {
                vgDetails.setVisibility(View.GONE);
            }
            return;
        }
        if (player.getExpandedView() == true || orientationHorizontal == true) {
            if (vgDetails.getVisibility() != View.VISIBLE) {
                // Expand
                ibToggle.setImageResource(R.drawable.beginner);
                Animation rotate = AnimationUtils.loadAnimation(context,
                        R.anim.rotate_90_clockwise);
                ibToggle.startAnimation(rotate);

                vgDetails.setVisibility(ViewGroup.VISIBLE);

                Animation dropDown = AnimationUtils.loadAnimation(context,
                        R.anim.drop_down);
                dropDown.setAnimationListener(new AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {
                        // Scroll the ScrollView to accommodate the expanded
                        // view.
                        Rect visibleRec = new Rect();
                        PlayerView.this.getGlobalVisibleRect(visibleRec);
                        int invisibleHeight = getHeight() - visibleRec.height();
                        if (invisibleHeight != 0) {
                            ScrollView scrollView = (ScrollView) getParent()
                                    .getParent();
                            // Scroll with 10dp space on either side
                            final int scrollBufferSpace = Utils.convertDpToPx(
                                    context, 10);
                            int top = getTop();
                            if (top < scrollView.getScrollY()) {
                                scrollView.smoothScrollTo(0, top
                                        - scrollBufferSpace);
                            } else {
                                scrollView.smoothScrollBy(0, invisibleHeight
                                        + scrollBufferSpace);
                            }
                        }
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {
                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                    }
                });
                vgDetails.startAnimation(dropDown);
            }
        } else {
            if (vgDetails.getVisibility() == View.VISIBLE) {
                // Collapse
                ibToggle.setImageResource(R.drawable.beginner_rotate_270);
                Animation rotate = AnimationUtils.loadAnimation(context,
                        R.anim.rotate_90_anticlockwise);
                ibToggle.startAnimation(rotate);

                Animation dropDown = AnimationUtils.loadAnimation(context,
                        R.anim.roll_up);
                dropDown.setAnimationListener(new AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {
                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        vgDetails.setVisibility(View.GONE);
                    }
                });
                vgDetails.startAnimation(dropDown);
            }
        }
    }

    private void populateBasics() {
        tvName.setText(player.getFullName());
        tvName.setSelected(true);
    }

    private void showSummary() {
        int summaryNumbersVisibility = View.VISIBLE;
        if (player.getPopulated() == false) {
            // We don't have the numbers for this player.
            summaryNumbersVisibility = View.INVISIBLE;
        }
        ibRemove.setVisibility(summaryNumbersVisibility);
        tvRatingText.setVisibility(summaryNumbersVisibility);
        tvRating.setVisibility(summaryNumbersVisibility);
        tvKDRText.setVisibility(summaryNumbersVisibility);
        tvKDR.setVisibility(summaryNumbersVisibility);
        tvTSRText.setVisibility(summaryNumbersVisibility);
        tvTSR.setVisibility(summaryNumbersVisibility);
    }

    public void populateStats(GameMode gameMode) {
        reflectStar();
        reflectState(player.getState());

        String playerUpdatedTimestamp = player.getTimestamp();
        if (previousGameMode == gameMode
                && playerUpdatedTimestamp
                        .compareTo(this.playerUpdatedTimestamp) == 0) {
            // Values are up to date.
            return;
        }

        populateBasics();
        showSummary();
        showDetails();
        invalidate();

        if (player.getPopulated() == false) {
            return;
        }
        this.playerUpdatedTimestamp = playerUpdatedTimestamp;
        previousGameMode = gameMode;

        tvRating.setText(new DecimalFormat("####").format(player
                .getRating(gameMode)));
        highlightView(vBorderRating, player.isChanged(gameMode, Player.Rating),
                0);

        DecimalFormat df = new DecimalFormat();
        df.setMinimumFractionDigits(0);
        df.setMaximumFractionDigits(0);

        tvWins.setText(player.getWins(gameMode) + " ("
                + df.format(player.getWinsPercentage(gameMode)) + " %)");
        tvLosses.setText(player.getLosses(gameMode) + " ("
                + df.format(player.getLossesPercentage(gameMode)) + " %)");
        tvDisconnects.setText(player.getDisconnects(gameMode) + " ("
                + df.format(player.getDisconnectsPercentage(gameMode)) + " %)");
        highlightView(vBorderWLD,
                player.isChanged(gameMode, Player.WinsPercentage),
                R.drawable.list_item_border);

        tvKills.setText(player.getKills(gameMode).toString());
        tvDeaths.setText(player.getDeaths(gameMode).toString());
        tvAssists.setText(player.getAssists(gameMode).toString());

        df.setMinimumFractionDigits(2);
        df.setMaximumFractionDigits(2);

        tvKDR.setText(df.format(player.getKDR(gameMode)));
        highlightView(vBorderKDR, player.isChanged(gameMode, Player.KDR), 0);

        tvKADR.setText(df.format(player.getKADR(gameMode)));

        if (gameMode == GameMode.Pub) {
            tvRatingText.setText(context.getString(R.string.psr));
        } else {
            tvRatingText.setText(context.getString(R.string.mmr));
        }

        tvTSR.setTextColor(Player.getTsrColour(context,
                player.getTSR(gameMode), player.getGames(gameMode)));
        tvTSR.setText(df.format(player.getTSR(gameMode)));
        highlightView(vBorderTSR, player.isChanged(gameMode, Player.TSR), 0);

        tvAPM.setText(df.format(player.getAPM(gameMode)));
        tvXPPM.setText(df.format(player.getXPPM(gameMode)));
        tvGPM.setText(df.format(player.getGPM(gameMode)));
        tvWPM.setText(df.format(player.getWPM(gameMode)));
    }

    private void highlightView(View view, Integer changed, int alternateResource) {
        if (player.retrievedUpdate() && changed != null
                && changed.floatValue() != 0) {
            if (changed.intValue() < 0) {
                // Value increased
                view.setBackgroundResource(R.drawable.value_highlight_positive);
            } else {
                // Value decreased
                view.setBackgroundResource(R.drawable.value_highlight_negative);
            }
        } else {
            view.setBackgroundResource(alternateResource);
        }
    }

    public void reflectStar() {
        if (PlayerView.this.player.getStarred() == true) {
            playerView
                    .setBackgroundResource(R.drawable.list_item_border_starred_with_background);
            ibStar.setImageResource(R.drawable.btn_star_big_on);
        } else {
            playerView
                    .setBackgroundResource(R.drawable.list_item_border_with_background);
            ibStar.setImageResource(R.drawable.btn_star_big_off);
        }
    }

    public void addToView(ViewGroup parent) {
        ViewGroup oldParent = (ViewGroup) getParent();
        if (oldParent != parent) {
            if (oldParent != null) {
                oldParent.removeView(this);
            }
            if (parent != null) {
                parent.addView(this);
            }
            setVisibility(View.VISIBLE);
        }
    }

    public void removeFromParent() {
        Global.animateShrink(this, context, true);
    }

    public void reflectState(State state) {
        if (tvPlayerStatus != null) {
            tvPlayerStatus.setText("");

            switch (state) {
            case Idle:
                Global.animateFadeOut(tvPlayerStatus, context, false);
                tvPlayerStatus.setTextColor(context.getResources().getColor(
                        R.color.idle));
                break;
            case Queued:
                tvPlayerStatus.setText("Queued...");
                Global.animateFadeIn(tvPlayerStatus, context);
                tvPlayerStatus.setTextColor(context.getResources().getColor(
                        R.color.queued));
                break;
            case InProgress:
                tvPlayerStatus.setText("Updating...");
                Global.animateFadeIn(tvPlayerStatus, context);
                tvPlayerStatus.setTextColor(context.getResources().getColor(
                        R.color.in_progress));
                break;
            case Incomplete:
                Global.animateFadeOut(tvPlayerStatus, context, false);
                tvPlayerStatus.setTextColor(context.getResources().getColor(
                        R.color.idle));
                break;
            default:
                Global.animateFadeOut(tvPlayerStatus, context, false);
                tvPlayerStatus.setTextColor(context.getResources().getColor(
                        R.color.idle));
                Log.e(Global.TAG, "PlayerView.reflectState - Unknown state '"
                        + state.toString() + "'.");
                break;
            }
        }
    }

    public boolean isOrientationHorizontal() {
        return orientationHorizontal;
    }
}
