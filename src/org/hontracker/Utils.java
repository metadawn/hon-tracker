package org.hontracker;

import java.util.zip.Deflater;
import java.util.zip.Inflater;

import android.content.Context;
import android.util.Log;

public class Utils {
    public static final String TAG = "HonTracker";

    static public byte[] compress(final String source) {
        Deflater deflater = new Deflater();
        byte[] input = source.getBytes();
        deflater.setInput(input);
        deflater.finish();

        byte[] destination = new byte[2 * input.length];
        deflater.deflate(destination);
        deflater.end();

        return destination;
    }

    static public byte[] uncompress(final byte[] source) {
        Inflater inflater = new Inflater();
        inflater.setInput(source);
        byte[] uncompressed = new byte[25 * source.length];
        try {
            inflater.inflate(uncompressed);
        } catch (Exception e) {
            Log.e(TAG + " uncompress", e.toString());
        }
        inflater.end();
        return uncompressed;
    }

    static public int convertDpToPx(Context context, int dpValue) {
        return (int) (context.getResources().getDisplayMetrics().density
                * dpValue + 0.5f);
    }
}
